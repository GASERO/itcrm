<body>
<style>
    /* внешние границы таблицы серого цвета толщиной 1px */
    table {border: 1px solid grey;}
    /* границы ячеек первого ряда таблицы */
    th {border: 1px solid grey; text-align: center}
    /* границы ячеек тела таблицы */
    td {border: 1px solid grey;}
</style>
<h4>
    <a href="/admin/tasks/new">Добавить</a>
</h4>

<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th></th>
    </tr>
<#list model.tasks as task>
    <tr>
        <td>${task.id}</td>
        <td>${task.name}</td>
        <td>
            <form method="post" action="/admin/tasks/${task.id}/delete">
                <button class = "option">delete</button>
            </form>
        </td>
        <td>
            <a href="/admin/tasks/${task.id}/update">edit</a>
        </td>
        <td>
            <#list task.executors as ex>
                ${ex.user.name}
            </#list>
        </td>
    </tr>
</#list>
</table>
</body>