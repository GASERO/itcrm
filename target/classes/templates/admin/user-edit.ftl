<div>
    <form action="/admin/users/${model.user.id}/update" method="post">
        <input type="text" readonly placeholder="Project Name" name="id" value="${model.user.id}">
        <input type="text" placeholder="Login" name="login" value="${model.user.login}">
        <input type="text" placeholder="Name" name="name" value="${model.user.user.name}">
        <select name="role" title="Role">
            <option <#if model.user.role == 'ADMIN'>selected</#if> value="ADMIN">ADMIN</option>
            <option <#if model.user.role == 'MODER'>selected</#if> value="MODER">MODER</option>
            <option <#if model.user.role == 'USER'>selected</#if> value="USER">USER</option>
        </select>
        <input type="submit" value="save">
    </form>
</div>