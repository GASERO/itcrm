<div>
    <form action="/admin/tasks/${model.task.id}/update" method="post">
        <input type="text" placeholder="Task Name" name="name" value="${model.task.name}">
        <input type="text" placeholder="Task Desc" name="description" value="${model.task.description}">

        <input type="text" name="projectId" hidden value="${model.task.project.id}">
        <input type="text" name="creatorId" hidden value="${model.task.creator.id}">

        <select required multiple="multiple" name="executorsId"  title="Executors">
               <#list model.users as userData>
                   <option <#if model.task.executors?seq_contains(userData)>selected</#if> value="${userData.id}"> ${userData.user.name}</option>
               </#list>
        </select>
        <input type="date" name="deadLine" placeholder="Deadline" value="2018-05-18">
        <input type="submit" value="save">
    </form>
</div>