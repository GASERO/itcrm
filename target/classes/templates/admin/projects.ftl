<body>
<style>
    /* внешние границы таблицы серого цвета толщиной 1px */
    table {border: 1px solid grey;}
    /* границы ячеек первого ряда таблицы */
    th {border: 1px solid grey; text-align: center}
    /* границы ячеек тела таблицы */
    td {border: 1px solid grey;}
</style>
<h4>
    <a href="/admin/projects/new">Добавить</a>
</h4>

<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th></th>
    </tr>
<#list model.projects as proj>
    <tr>
        <td>${proj.id}</td>
        <td>${proj.name}</td>
        <td>
            <form method="post" action="/admin/projects/${proj.id}/delete">
                <button class = "option">delete</button>
            </form>
        </td>
        <td>
            <a href="/admin/projects/${proj.id}/update">edit</a>
        </td>
    </tr>
</#list>
</table>
</body>