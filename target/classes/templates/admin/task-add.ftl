<div>
    <form action="/admin/tasks/new" method="post">
        <input type="text" placeholder="Task Name" name="name">
        <input type="text" placeholder="Task Desc" name="description">
        <select name="projectId" title="Project">
        <#list model.projects as proj>
            <option value="${proj.id}"> ${proj.name}</option>
        </#list>
        </select>

        <select required multiple="multiple" name="executorsId"  title="Executors">
               <#list model.users as userData>
                   <option value="${userData.id}"> ${userData.user.name}</option>
               </#list>
        </select>
        <input type="date" name="deadLine" placeholder="Deadline">
        <input type="submit" value="save">
    </form>
</div>