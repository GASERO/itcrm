<div>
    <form action="/admin/projects/new" method="post">
        <input type="text" placeholder="Project Name" name="name" value="${model.project.name}">
        <select name="managerId" title="Project Manager">
        <#list model.users as user>
            <option <#if user.id == model.project.manager.id>selected</#if> value="${user.id}"> ${user.user.name}</option>
        </#list>
            <input type="submit" value="save">
        </select>
    </form>
</div>