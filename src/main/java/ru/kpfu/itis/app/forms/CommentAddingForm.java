package ru.kpfu.itis.app.forms;

import lombok.*;
import ru.kpfu.itis.app.model.UserData;

import java.util.Date;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CommentAddingForm {
    private Date date;
    private Long authorId;
    private String text;
}
