package ru.kpfu.itis.app.forms;

import lombok.*;

import java.util.Date;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MembershipAddingForm {
    private Long id;
    private String name;
    private Date dateOfCreation;
}
