package ru.kpfu.itis.app.forms;

import lombok.*;

import java.util.Date;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TaskAddingForm {
    private Long id;
    private String name;
    private String description;
    private Long creatorId;
    private Long projectId;
    private List<Long> executorsId;
    private Date dateOfCreation;
    private String deadLine;
}
