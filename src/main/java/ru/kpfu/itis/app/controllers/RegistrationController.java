package ru.kpfu.itis.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.services.RegistrationService;
import ru.kpfu.itis.app.validators.UserRegistrationFormValidator;


import javax.validation.Valid;

@Controller
public class RegistrationController {

    private RegistrationService service;
    private UserRegistrationFormValidator userRegistrationFormValidator;

    public RegistrationController(RegistrationService service, UserRegistrationFormValidator userRegistrationFormValidator) {
        this.service = service;
        this.userRegistrationFormValidator = userRegistrationFormValidator;
    }

    @InitBinder("userForm")
    public void initUserFormValidator(WebDataBinder binder) {
        binder.addValidators(userRegistrationFormValidator);
    }
    @PostMapping(value = "/registration")
    public String signUp(@Valid @ModelAttribute("userForm") UserRegistrationForm userRegistrationForm,
                         BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/registration";
        }
        service.register(userRegistrationForm);
        return "redirect:/user/profile";
    }

    @GetMapping(value = "/registration")
    public String getSignUpPage() {
        return "page-register";
    }
}