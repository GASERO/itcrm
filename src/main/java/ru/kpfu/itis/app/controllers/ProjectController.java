package ru.kpfu.itis.app.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.app.model.Project;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.model.status.TaskStatus;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.ProjectService;
import ru.kpfu.itis.app.services.TaskService;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 29.04.2018
 */
@Controller
@RequestMapping("user/projects")
public class ProjectController {
    private AuthenticationService authenticationService;
    private TaskService taskService;
    private ProjectService projectService;

    public ProjectController(AuthenticationService authenticationService, TaskService taskService, ProjectService projectService) {
        this.authenticationService = authenticationService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @GetMapping("")
    public String getProjectsPage(@ModelAttribute("model") ModelMap model,
                                  Authentication authentication){
        UserData userData = authenticationService.getUserByAuthentication(authentication);
        model.addAttribute("userData",userData);
        model.addAttribute("activeTasks",taskService.getAllActiveForUser(userData));
        return "projects";
    }

    @GetMapping("/{project-id}")
    public String getProject(@ModelAttribute("model") ModelMap model,
                                  @PathVariable("project-id") Long id,
                                  Authentication authentication){
        UserData userData = authenticationService.getUserByAuthentication(authentication);
        model.addAttribute("userData",userData);

        Project project = projectService.getById(id);
        model.addAttribute("project",project);
        model.addAttribute(TaskStatus.ACTIVE.toString(),taskService.getAllActiveTasksForProject(project));
        model.addAttribute(TaskStatus.TODO.toString(),taskService.getAllTodoTasksForProject(project));
        model.addAttribute(TaskStatus.FINISHED.toString(),taskService.getAllFinishedTasksForProject(project));
        model.addAttribute(TaskStatus.REVIEW.toString(),taskService.getAllReviewTasksForProject(project));
        return "project";
    }
}
