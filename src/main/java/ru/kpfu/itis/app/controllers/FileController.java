package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.kpfu.itis.app.services.FileStorageService;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 29.04.2018
 */
@Controller
public class FileController {

    @Autowired
    private FileStorageService service;


    @GetMapping("/files/{file-name:.+}")
    public void getFile(@PathVariable("file-name") String fileName,
                        HttpServletResponse response) {
        service.writeFileToResponse(fileName, response);
    }
}
