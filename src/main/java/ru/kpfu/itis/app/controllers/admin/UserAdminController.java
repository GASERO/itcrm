package ru.kpfu.itis.app.controllers.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.services.UserDataService;

import javax.validation.Valid;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 05.06.2018
 */
@Controller
@RequestMapping("/admin/users")
public class UserAdminController {
    private UserDataService userDataService;

    public UserAdminController(UserDataService userDataService) {
        this.userDataService = userDataService;
    }

    @GetMapping("")
    public String getAllUsers(@ModelAttribute("model") ModelMap model){
        model.addAttribute("users", userDataService.getAll());
        return "admin/users";
    }

    @GetMapping("/{id}")
    public String getUserInfo(@ModelAttribute("model") ModelMap model, @PathVariable("id") Long id){
        model.addAttribute("user",userDataService.getById(id));
        return "admin/user";
    }

    @PostMapping("/{id}/delete")
    public String deleteUser(@ModelAttribute("model") ModelMap model, @PathVariable("id") Long id){
        userDataService.delete(id);
        return "redirect:/admin/users";
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable("id") Long id,
                                @Valid @ModelAttribute("userAddingForm")UserEditForm userEditForm){
        userDataService.update(userEditForm);
        return "redirect:/admin/users";
    }

    @GetMapping("/{id}/update")
    public String getUpdateUserPage(@PathVariable("id") Long id,
                                       @ModelAttribute("model") ModelMap model){
        model.addAttribute("users", userDataService.getAll());
        model.addAttribute("user",userDataService.getById(id));
        return "admin/user-edit";
    }

    @PostMapping("/new")
    public String saveUser(@Valid @ModelAttribute("userAddingForm")UserRegistrationForm userRegistrationForm){
        return "redirect:/admin/users";
    }

    @GetMapping("/new")
    public String getSavePage(@ModelAttribute("model") ModelMap model){
        model.addAttribute("users", userDataService.getAll());
        return "admin/user-add";
    }
}
