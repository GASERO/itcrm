package ru.kpfu.itis.app.controllers.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.app.services.TaskService;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 04.05.2018
 */
@RestController
@RequestMapping("/rest/task")
public class TaskController {
    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/add")
    public String addNewTask(){
        return null;
    }

}
