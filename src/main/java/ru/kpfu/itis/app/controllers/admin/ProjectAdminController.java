package ru.kpfu.itis.app.controllers.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.app.forms.ProjectAddingForm;
import ru.kpfu.itis.app.services.ProjectService;
import ru.kpfu.itis.app.services.UserDataService;

import javax.validation.Valid;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 05.06.2018
 */
@Controller
@RequestMapping("/admin/projects")
public class ProjectAdminController {
    private ProjectService projectService;
    private UserDataService userDataService;

    public ProjectAdminController(ProjectService projectService, UserDataService userDataService) {
        this.projectService = projectService;
        this.userDataService = userDataService;
    }

    @GetMapping("")
    public String getAllProjects(@ModelAttribute("model") ModelMap model){
        model.addAttribute("projects",projectService.getAll());
        return "admin/projects";
    }

    @GetMapping("/{id}")
    public String getProjectInfo(@ModelAttribute("model") ModelMap model, @PathVariable("id") Long id){
        model.addAttribute("project",projectService.getById(id));
        return "admin/project";
    }

    @PostMapping("/{id}/delete")
    public String deleteProject(@ModelAttribute("model") ModelMap model, @PathVariable("id") Long id){
        projectService.delete(id);
        return "redirect:/admin/projects";
    }

    @PostMapping("/{id}/update")
    public String updateProject(@PathVariable("id") Long id,
                                @Valid @ModelAttribute("projectAddingForm")ProjectAddingForm projectAddingForm){
        projectAddingForm.setId(id);
        projectService.save(projectAddingForm);
        return "redirect:/admin/projects";
    }

    @GetMapping("/{id}/update")
    public String getUpdateProjectPage(@PathVariable("id") Long id,
                                       @ModelAttribute("model") ModelMap model){
        model.addAttribute("users", userDataService.getAll());
        model.addAttribute("project",projectService.getById(id));
        return "admin/project-edit";
    }

    @PostMapping("/new")
    public String saveProject(@Valid @ModelAttribute("projectAddingForm")ProjectAddingForm projectAddingForm){
        projectService.save(projectAddingForm);
        return "redirect:/admin/projects";
    }

    @GetMapping("/new")
    public String getSavePage(@ModelAttribute("model") ModelMap model){
        model.addAttribute("users", userDataService.getAll());
        return "admin/project-add";
    }
}
