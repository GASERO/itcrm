package ru.kpfu.itis.app.controllers.admin;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.app.forms.TaskAddingForm;
import ru.kpfu.itis.app.services.AuthenticationService;
import ru.kpfu.itis.app.services.ProjectService;
import ru.kpfu.itis.app.services.TaskService;
import ru.kpfu.itis.app.services.UserDataService;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 05.06.2018
 */
@Controller
@RequestMapping("/admin/tasks")
public class TaskAdminController {
    private TaskService taskService;
    private UserDataService userDataService;
    private ProjectService projectService;
    private AuthenticationService authenticationService;

    public TaskAdminController(TaskService taskService, UserDataService userDataService, ProjectService projectService, AuthenticationService authenticationService) {
        this.taskService = taskService;
        this.userDataService = userDataService;
        this.projectService = projectService;
        this.authenticationService = authenticationService;
    }

    @GetMapping("")
    public String getAllTasks(@ModelAttribute("model") ModelMap model){
        model.addAttribute("tasks",taskService.getAll());
        return "admin/tasks";
    }

    @GetMapping("/{id}")
    public String getTaskInfo(@ModelAttribute("model") ModelMap model, @PathVariable("id") Long id){
        model.addAttribute("task",taskService.getById(id));
        return "admin/task";
    }

    @PostMapping("/{id}/delete")
    public String deleteTask(@ModelAttribute("model") ModelMap model, @PathVariable("id") Long id){
        taskService.delete(id);
        return "redirect:/admin/tasks";
    }

    @PostMapping("/{id}/update")
    public String updateTask(@PathVariable("id") Long id,
                             @ModelAttribute("taskAddingForm")TaskAddingForm taskAddingForm){
        taskService.save(taskAddingForm);
        return "redirect:/admin/tasks";
    }

    @GetMapping("/{id}/update")
    public String getUpdateTaskPage(@PathVariable("id") Long id,
                                    @ModelAttribute("model") ModelMap model){
        model.addAttribute("users", userDataService.getAll());
        model.addAttribute("task",taskService.getById(id));
        return "admin/task-edit";
    }

    @PostMapping("/new")
    public String saveTask(@ModelAttribute("taskAddingForm")TaskAddingForm taskAddingForm,Authentication authentication){
        System.out.println(taskAddingForm);
        taskAddingForm.setCreatorId(authenticationService.getUserByAuthentication(authentication).getId());
        taskService.save(taskAddingForm);
        return "redirect:/admin/tasks";
    }

    @GetMapping("/new")
    public String getSavePage(@ModelAttribute("model") ModelMap model){
        model.addAttribute("users", userDataService.getAll());
        model.addAttribute("projects",projectService.getAll());
        return "admin/task-add";
    }
}
