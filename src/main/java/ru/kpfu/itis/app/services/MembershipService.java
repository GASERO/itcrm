package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.model.Membership;
import ru.kpfu.itis.app.model.Project;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.forms.MembershipAddingForm;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
public interface MembershipService {
    List<Membership> getAll();
    void add(MembershipAddingForm roleInProjectAddingForm);
    Membership getById(Long id);
    void delete(Long id);

    void deleteAllForProject(Long id);

}
