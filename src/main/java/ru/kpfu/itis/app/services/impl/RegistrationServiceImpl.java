package ru.kpfu.itis.app.services.impl;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.UserRegistrationForm;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.services.RegistrationService;
import ru.kpfu.itis.app.services.UserDataService;


@Service
public class RegistrationServiceImpl implements RegistrationService {
    private UserDataService userDataService;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public RegistrationServiceImpl(UserDataService userDataService) {
        this.userDataService = userDataService;
    }

    @Override
    public void register(UserRegistrationForm userForm) {
        UserData newUserData = UserData.builder()
                .login(userForm.getLogin())
                .hashPassword(passwordEncoder.encode(userForm.getPassword()))
                .role(Role.USER)
                .user(User.builder()
                        .name(userForm.getName())
                .build())
                .build();
        userDataService.save(newUserData);
    }
}