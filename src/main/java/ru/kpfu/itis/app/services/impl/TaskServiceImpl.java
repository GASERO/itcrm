package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.TaskAddingForm;
import ru.kpfu.itis.app.model.Project;
import ru.kpfu.itis.app.model.Task;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.model.status.TaskStatus;
import ru.kpfu.itis.app.repositories.TaskRepository;
import ru.kpfu.itis.app.services.ProjectService;
import ru.kpfu.itis.app.services.TaskService;
import ru.kpfu.itis.app.services.UserDataService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private ProjectService projectService;


    @Override
    public List<Task> getAll() {
        return taskRepository.findAll();
    }

    @Override
    public void add(TaskAddingForm taskAddingForm) {
        taskRepository.save(Task.builder()
                .build());
    }

    @Override
    public Task getById(Long id) {
        return taskRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        taskRepository.delete(id);
    }

    @Override
    public List<Task> getAllActiveTasksForProject(Project project) {
        return taskRepository.getAllByProjectAndStatus(project,TaskStatus.ACTIVE);
    }

    @Override
    public List<Task> getAllFinishedTasksForProject(Project project) {
        return taskRepository.getAllByProjectAndStatus(project,TaskStatus.FINISHED);
    }

    @Override
    public List<Task> getAllReviewTasksForProject(Project project) {
        return taskRepository.getAllByProjectAndStatus(project,TaskStatus.REVIEW);
    }

    @Override
    public List<Task> getAllTodoTasksForProject(Project project) {
        return taskRepository.getAllByProjectAndStatus(project,TaskStatus.TODO);
    }

    @Override
    public List<Task> getAllActiveForUser(UserData userData) {
        return taskRepository.getAllByExecutorsContainsAndStatus(userData, TaskStatus.ACTIVE);
    }

    @Override
    public void save(TaskAddingForm taskAddingForm) {
        try {
            taskRepository.save(Task.builder()
                    .id(taskAddingForm.getId())
                    .creator(userDataService.getById(taskAddingForm.getCreatorId()))
                    .dateOfCreation(new Date())
                    .name(taskAddingForm.getName())
                    .deadLine((new SimpleDateFormat("yyyy-mm-dd")).parse(taskAddingForm.getDeadLine()))
                    .description(taskAddingForm.getDescription())
                    .executors(userDataService.getAll(taskAddingForm.getExecutorsId()))
                    .project(projectService.getById(taskAddingForm.getProjectId()))
                    .status(TaskStatus.ACTIVE)
                    .build());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
