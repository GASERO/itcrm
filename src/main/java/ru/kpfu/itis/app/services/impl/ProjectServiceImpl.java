package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.app.forms.ProjectAddingForm;
import ru.kpfu.itis.app.model.Project;
import ru.kpfu.itis.app.model.status.ProjectStatus;
import ru.kpfu.itis.app.repositories.ProjectRepository;
import ru.kpfu.itis.app.services.MembershipService;
import ru.kpfu.itis.app.services.ProjectService;
import ru.kpfu.itis.app.services.UserDataService;

import java.util.Date;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private MembershipService membershipService;

    @Autowired
    private UserDataService userDataService;


    @Override
    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public void add(ProjectAddingForm projectAddingForm) {
        projectRepository.save(Project.builder()
                .build());
    }

    @Override
    public Project getById(Long id) {
        return projectRepository.findOne(id);
}

    @Transactional
    @Override
    public void delete(Long id) {
        membershipService.deleteAllForProject(id);
        projectRepository.delete(id);
    }

    @Override
    public void save(ProjectAddingForm projectAddingForm) {
        projectRepository.save(Project.builder()
                .manager(userDataService.getById(projectAddingForm.getManagerId()))
                .dateOfCreation(new Date())
                .name(projectAddingForm.getName())
                .status(ProjectStatus.ACTIVE)
                .build());
    }
}
