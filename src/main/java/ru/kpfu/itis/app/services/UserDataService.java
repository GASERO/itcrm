package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.model.UserData;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 19.03.2018
 */
public interface UserDataService {
    List<UserData> getAll();
    void update(UserEditForm userEditForm);
    UserData getById(Long id);
    void delete(Long id);

    List<UserData> getAll(List<Long> executorsId);

    void save(UserData newUserData);
}
