package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.repositories.UserDataRepository;
import ru.kpfu.itis.app.repositories.UsersRepository;
import ru.kpfu.itis.app.security.role.Role;
import ru.kpfu.itis.app.security.status.UserStatus;
import ru.kpfu.itis.app.services.UserDataService;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 19.03.2018
 */
@Service
public class UserDataServiceImpl implements UserDataService {
    @Autowired
    private UserDataRepository userDatasRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public List<UserData> getAll() {
        return userDatasRepository.findAll();
    }

    @Override
    public void update(UserEditForm userEditForm) {
        System.out.println(userEditForm);
        userDatasRepository.save(UserData.builder()
                .id(Long.parseLong(userEditForm.getId()))
                .login(userEditForm.getLogin())
                .role(Role.valueOf(userEditForm.getRole()))
                .user(User.builder()
                        .name(userEditForm.getName())
                        .build())
                .build());
    }

    @Override
    public UserData getById(Long id) {
        return userDatasRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        Long userId = userDatasRepository.findOne(id).getUser().getId();
        userDatasRepository.delete(id);
        usersRepository.delete(userId);
    }

    @Override
    public List<UserData> getAll(List<Long> executorsId) {
        return userDatasRepository.findAll(executorsId);
    }

    @Override
    public void save(UserData newUserData) {
        userDatasRepository.save(newUserData);
    }
}
