package ru.kpfu.itis.app.services;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 29.04.2018
 */
public interface FileStorageService {
    String saveFile(MultipartFile file);
    void writeFileToResponse(String fileName, HttpServletResponse response);
}
