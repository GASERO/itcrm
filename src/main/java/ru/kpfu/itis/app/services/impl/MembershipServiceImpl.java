package ru.kpfu.itis.app.services.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.MembershipAddingForm;
import ru.kpfu.itis.app.model.Membership;
import ru.kpfu.itis.app.model.Project;
import ru.kpfu.itis.app.repositories.MembershipRepository;
import ru.kpfu.itis.app.services.MembershipService;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
@Service
public class MembershipServiceImpl implements MembershipService {
    private MembershipRepository roleInProjectRepository;

    public MembershipServiceImpl(MembershipRepository roleInProjectRepository) {
        this.roleInProjectRepository = roleInProjectRepository;
    }


    @Override
    public List<Membership> getAll() {
        return roleInProjectRepository.findAll();
    }

    @Override
    public void add(MembershipAddingForm roleInProjectAddingForm) {
        roleInProjectRepository.save(Membership.builder()
                .build());
    }

    @Override
    public Membership getById(Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void deleteAllForProject(Long id) {
        roleInProjectRepository.deleteAllByProjectId(id);
    }
}
