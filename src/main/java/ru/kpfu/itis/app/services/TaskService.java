package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.model.Project;
import ru.kpfu.itis.app.model.Task;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.forms.TaskAddingForm;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
public interface TaskService {
    List<Task> getAll();
    void add(TaskAddingForm taskAddingForm);
    Task getById(Long id);
    void delete(Long id);
    List<Task> getAllActiveTasksForProject(Project project);
    List<Task> getAllFinishedTasksForProject(Project project);
    List<Task> getAllReviewTasksForProject(Project project);
    List<Task> getAllTodoTasksForProject(Project project);
    List<Task> getAllActiveForUser(UserData userData);

    void save(TaskAddingForm taskAddingForm);
}
