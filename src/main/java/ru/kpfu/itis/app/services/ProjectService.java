package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.forms.ProjectAddingForm;
import ru.kpfu.itis.app.forms.UserEditForm;
import ru.kpfu.itis.app.model.Project;
import ru.kpfu.itis.app.model.UserData;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
public interface ProjectService {
    List<Project> getAll();
    void add(ProjectAddingForm projectAddingForm);
    Project getById(Long id);
    void delete(Long id);

    void save(ProjectAddingForm projectAddingForm);

}
