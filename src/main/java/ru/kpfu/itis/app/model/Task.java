package ru.kpfu.itis.app.model;

import lombok.*;
import org.apache.catalina.User;
import ru.kpfu.itis.app.model.status.TaskStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 26.04.2018
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
@Table(name = "task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    private UserData creator;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "task_user",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<UserData> executors;

 /*   @OneToMany(fetch = FetchType.LAZY, mappedBy = "task")
    private List<Comment> comments;
*/
    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    private Date dateOfCreation;

    private Date deadLine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;


}
