package ru.kpfu.itis.app.model.role;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 29.04.2018
 */
public enum ProjectRole {
    DEVELOPER, MANAGER, TESTER, DESIGNER, ANALYST, TECHLEADER
}
