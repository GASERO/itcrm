package ru.kpfu.itis.app.model.status;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 26.04.2018
 */
public enum CommentStatus {
    DELETED, POSTED
}
