package ru.kpfu.itis.app.model;

import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.app.model.status.ProjectStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 26.04.2018
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "project")
    private List<Membership> members;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manager_id")
    private UserData manager;

    private Date dateOfCreation;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "project",
    cascade = CascadeType.REMOVE)
    private List<Task> tasks;

    @Enumerated(EnumType.STRING)
    private ProjectStatus status;

}
