package ru.kpfu.itis.app.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.kpfu.itis.app.forms.TaskAddingForm;
import ru.kpfu.itis.app.repositories.TaskRepository;
import ru.kpfu.itis.app.repositories.UserDataRepository;

import java.util.Optional;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
@Component
public class TaskAddingFormValidator implements Validator {

    @Autowired
    private TaskRepository taskRepository;



    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(TaskAddingForm.class.getName());
    }

    @Transactional
    @Override
    public void validate(Object target, Errors errors) {

    }
}