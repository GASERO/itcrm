package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.FileInfo;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 29.04.2018
 */
public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {
    FileInfo findOneByStorageFileName(String storageFileName);
}
