package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.Project;
import ru.kpfu.itis.app.model.Task;
import ru.kpfu.itis.app.model.User;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.model.status.TaskStatus;

import java.util.List;
import java.util.Optional;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> getAllByExecutorsContainsAndStatus(UserData executor, TaskStatus status);

    List<Task> getAllByProjectAndStatus(Project project,TaskStatus taskStatus);
}