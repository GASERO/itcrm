package ru.kpfu.itis.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.app.model.Membership;
import ru.kpfu.itis.app.model.Project;

import java.util.Optional;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 28.04.2018
 */
public interface MembershipRepository extends JpaRepository<Membership, Long> {
    void deleteAllByProjectId(Long id);
}