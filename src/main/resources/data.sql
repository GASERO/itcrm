INSERT INTO "user"(name)
  SELECT 'Administrator'
  WHERE NOT EXISTS(
      SELECT id
      FROM "user"
      WHERE id = 1
  );
INSERT INTO userdata(login, hash_password, role, user_status, user_id)
  SELECT 'admin', '$2a$10$1jcIqkeEGVX.Lqz5zMLZFeuGQkxiwxUciBjzBleGXcnplIOn4A/4m', 'ADMIN', 'CONFIRMED', 1
  WHERE NOT EXISTS(
      SELECT id
      FROM userdata
      WHERE id = 1
  );

INSERT INTO "user"(name)
  SELECT 'Moderator'
  WHERE NOT EXISTS(
      SELECT id
      FROM "user"
      WHERE id = 2
  );
INSERT INTO userdata(login, hash_password, role, user_status, user_id)
  SELECT 'moder', '$2a$10$1jcIqkeEGVX.Lqz5zMLZFeuGQkxiwxUciBjzBleGXcnplIOn4A/4m', 'MODER', 'CONFIRMED', 2
  WHERE NOT EXISTS(
      SELECT id
      FROM userdata
      WHERE id = 2
  );

INSERT INTO "user"(name)
  SELECT 'TestUser'
  WHERE NOT EXISTS(
      SELECT id
      FROM "user"
      WHERE id = 3
  );
INSERT INTO userdata(login, hash_password, role, user_status, user_id)
  SELECT 'testUser', '$2a$10$1jcIqkeEGVX.Lqz5zMLZFeuGQkxiwxUciBjzBleGXcnplIOn4A/4m', 'USER', 'CONFIRMED', 3
  WHERE NOT EXISTS(
      SELECT id
      FROM userdata
      WHERE id = 3
  );