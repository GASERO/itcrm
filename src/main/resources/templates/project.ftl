<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon.png">
    <title>Projects</title>
    <!-- Bootstrap Core CSS -->
    <link href="/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/helper.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/lib/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .ololo{
            background-color: #f2f2f2;
            padding-left: 5px;
            padding-right: 5px;
            margin-left: 5px;
            margin-right: 5px;
        }

    </style>

</head>

<body class="fix-header fix-sidebar">
<!-- Preloader - style you can find in spinners.css -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>


<!-- Modal -->
<div class="modal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" id="addTaskModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" novalidate="novalidate">
                <div class="modal-header">
                    <h5 class="modal-title">Add Task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="">
                        <div class="form-group">
                            <form>
                                <label for="taskName">
                                    Task
                                </label>


                                    <textarea style="resize: vertical; height: 100px" class="form-control"
                                              id="exampleTextarea" rows="4" placeholder="Task"></textarea>
                                <label for="example-datetime-local-input" >Deadline</label>
                                <div class="col-10">
                                    <input class="form-control" type="datetime-local" id="example-datetime-local-input">
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal -->

<!-- Main wrapper  -->
<div id="main-wrapper">
    <!-- header header  -->
    <div class="header">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- Logo -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <!-- Logo icon -->
                    <b><img src="/images/logo.png" alt="homepage" class="dark-logo"/></b>
                    <!--End Logo icon -->
                    <!-- Logo text -->
                    <span><img src="/images/logo-text.png" alt="homepage" class="dark-logo"/></span>
                </a>
            </div>
            <!-- End Logo -->
            <div class="navbar-collapse">
                <!-- toggle and nav items -->
                <ul class="navbar-nav mr-auto mt-md-0">
                    <!-- This is  -->
                    <li class="nav-item"><a class="nav-link nav-toggler hidden-md-up text-muted  "
                                            href="javascript:void(0)"><i class="mdi mdi-menu"></i></a></li>
                    <li class="nav-item m-l-10"><a class="nav-link sidebartoggler hidden-sm-down text-muted  "
                                                   href="javascript:void(0)"><i class="ti-menu"></i></a></li>
                    <!-- Messages -->
                    <li class="nav-item dropdown mega-dropdown"><a class="nav-link dropdown-toggle text-muted  "
                                                                   href="#" data-toggle="dropdown" aria-haspopup="true"
                                                                   aria-expanded="false"><i class="fa fa-th-large"></i></a>
                        <div class="dropdown-menu animated zoomIn">
                            <ul class="mega-dropdown-menu row">


                                <li class="col-lg-3  m-b-30">
                                    <h4 class="m-b-20">CONTACT US</h4>
                                    <!-- Contact -->
                                    <form>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="exampleInputname1"
                                                   placeholder="Enter Name"></div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Enter email"></div>
                                        <div class="form-group">
                                            <textarea class="form-control" id="exampleTextarea" rows="3"
                                                      placeholder="Message"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </form>
                                </li>
                                <li class="col-lg-3 col-xlg-3 m-b-30">
                                    <h4 class="m-b-20">List style</h4>
                                    <!-- List style -->
                                    <ul class="list-style-none">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                    </ul>
                                </li>
                                <li class="col-lg-3 col-xlg-3 m-b-30">
                                    <h4 class="m-b-20">List style</h4>
                                    <!-- List style -->
                                    <ul class="list-style-none">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                    </ul>
                                </li>
                                <li class="col-lg-3 col-xlg-3 m-b-30">
                                    <h4 class="m-b-20">List style</h4>
                                    <!-- List style -->
                                    <ul class="list-style-none">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> This
                                            Is Another Link</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- End Messages -->
                </ul>
                <!-- User profile and search -->
                <ul class="navbar-nav my-lg-0">

                    <!-- Search -->
                    <li class="nav-item hidden-sm-down search-box"><a class="nav-link hidden-sm-down text-muted  "
                                                                      href="javascript:void(0)"><i
                            class="ti-search"></i></a>
                        <form class="app-search">
                            <input type="text" class="form-control" placeholder="Search here"> <a class="srh-btn"><i
                                class="ti-close"></i></a></form>
                    </li>
                    <!-- Comment -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i>
                            <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                            <ul>
                                <li>
                                    <div class="drop-title">Notifications</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-danger btn-circle m-r-10"><i class="fa fa-link"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>This is title</h5> <span class="mail-desc">Just see the my new admin!</span>
                                                <span class="time">9:30 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-success btn-circle m-r-10"><i class="ti-calendar"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>This is another title</h5> <span class="mail-desc">Just a reminder that you have event</span>
                                                <span class="time">9:10 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-info btn-circle m-r-10"><i class="ti-settings"></i>
                                            </div>
                                            <div class="mail-contnet">
                                                <h5>This is title</h5> <span class="mail-desc">You can customize this template as you want</span>
                                                <span class="time">9:08 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-primary btn-circle m-r-10"><i class="ti-user"></i></div>
                                            <div class="mail-contnet">
                                                <h5>This is another title</h5> <span class="mail-desc">Just see the my admin!</span>
                                                <span class="time">9:02 AM</span>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all
                                        notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- End Comment -->
                    <!-- Messages -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted  " href="#" id="2" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"> <i class="fa fa-envelope"></i>
                            <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" aria-labelledby="2">
                            <ul>
                                <li>
                                    <div class="drop-title">You have 4 new messages</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"><img src="/images/users/5.jpg" alt="user"
                                                                       class="img-circle"> <span
                                                    class="profile-status online pull-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>Michael Qin</h5> <span
                                                    class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"><img src="/images/users/2.jpg" alt="user"
                                                                       class="img-circle"> <span
                                                    class="profile-status busy pull-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>John Doe</h5> <span
                                                    class="mail-desc">I've sung a song! See you at</span> <span
                                                    class="time">9:10 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"><img src="/images/users/3.jpg" alt="user"
                                                                       class="img-circle"> <span
                                                    class="profile-status away pull-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>Mr. John</h5> <span class="mail-desc">I am a singer!</span> <span
                                                    class="time">9:08 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"><img src="/images/users/4.jpg" alt="user"
                                                                       class="img-circle"> <span
                                                    class="profile-status offline pull-right"></span></div>
                                            <div class="mail-contnet">
                                                <h5>Michael Qin</h5> <span
                                                    class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all
                                        e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- End Messages -->
                    <!-- Profile -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><img src="/files/${model.userData.user.photo.storageFileName}" alt="user"
                                                                           class="profile-pic"/></a>
                        <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                            <ul class="dropdown-user">
                                <li><a href="#"><i class="ti-user"></i> Profile</a></li>
                                <li><a href="#"><i class="ti-wallet"></i> Balance</a></li>
                                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                <li><a href="#"><i class="ti-settings"></i> Setting</a></li>
                                <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- End header header -->
    <!-- Left Sidebar  -->
    <div class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="nav-devider"></li>
                    <li class="nav-label">Home</li>
                    <li><a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span
                            class="hide-menu">Projects <span class="label label-rouded label-primary pull-right">${model.userData.user.memberships?size}</span></span></a>
                        <ul aria-expanded="false" class="collapse">
                            <#list model.userData.user.memberships as membership>
                                <li><a href="/user/projects/${membership.project.id}">${membership.project.name}</a></li>
                            </#list>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </div>
    <!-- End Left Sidebar  -->
    <!-- Page wrapper  -->
    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-primary">${model.project.name}</h3></div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Projects</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="col-lg-12">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tasks" role="tab">Tasks</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#members"
                                                role="tab">Members</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#settings"
                                                role="tab">Settings</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- tasks tab(the 1st) -->
                        <div class="tab-pane active" id="tasks" role="tabpanel">

                            <div class="row" style="padding-top: 0.5%">
                                <div style="float:right">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#addTaskModal" >
                                        + Add task
                                    </button>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 0.5%">
                                <!-- TODO TASK BEGIN -->
                                <div class="col-md-3">
                                    <div class="container-fluid ololo">
                                        TODO
                                        <#if model.TODO?size !=0>
                                            <div class="card p-30">
                                            <#list model.TODO as task>
                                                <div class="media">
                                                    <div class="meida-left media">
                                                        ${task.name}
                                                    </div>
                                                </div>
                                            </#list>
                                            </div>
                                        </#if>
                                    </div>
                                </div>
                                <!-- TODO TASK END -->
                                <!-- ACTIVE TASK BEGIN -->
                                <div class="col-md-3">
                                    <div class="container-fluid ololo" >
                                        ACTIVE
                                        <#if model.ACTIVE?size !=0>

                                            <#list model.ACTIVE as task>
                                            <div class="card p-30">
                                                <div class="media">
                                                    <div class="media-left media">
                                                        ${task.name}
                                                    </div>
                                                <div class="media-text-right media-body">
                                                    <img src="/files/${model.userData.user.photo.storageFileName}"
                                                         alt="user" style="width: 30px"
                                                         class="profile-pic"/>
                                                </div>

                                                </div>
                                             <div class="media-text-right">
                                                  USERNAME
                                              </div>
                                            </div>
                                            </#list>


                                        </#if>

                                    </div>
                                </div>
                                <!-- ACTIVE TASK END -->
                                <!-- REVIEW TASK BEGIN -->
                                <div class="col-md-3">
                                    <div class="container-fluid ololo">
                                        REVIEW
                                        <#if model.REVIEW?size !=0>
                                            <div class="card p-30">
                                            <#list model.REVIEW as task>
                                                <div class="media">
                                                    <div class="media-left media">
                                                        ${task.name}
                                                    </div>
                                                <div class="media-text-right media-body">
                                                    <img src="/files/${model.userData.user.photo.storageFileName}"
                                                         alt="user" style="width: 30px"
                                                         class="profile-pic"/>
                                                </div>

                                                </div>
                                             <div class="media-text-right">
                                                  USERNAME
                                              </div>
                                            </#list>

                                            </div>
                                        </#if>

                                    </div>
                                </div>
                                <!-- REVIEW TASK END -->
                                <!-- DONE TASK BEGIN -->
                                <div class="col-md-3">
                                    <div class="container-fluid ololo">
                                        DONE
                                        <#if model.FINISHED?size !=0>
                                            <div class="card p-30">
                                            <#list model.FINISHED as task>
                                                <div class="media">
                                                    <div class="media-left media">
                                                        ${task.name}
                                                    </div>
                                                <div class="media-text-right media-body">
                                                    <img src="/files/${model.userData.user.photo.storageFileName}"
                                                         alt="user" style="width: 30px"
                                                         class="profile-pic"/>
                                                </div>

                                                </div>
                                              <div class="media-text-right">
                                                  USERNAME
                                              </div>
                                            </#list>

                                            </div>
                                        </#if>

                                    </div>
                                </div>
                                <!-- DONE TASK END -->
                            </div>
                        </div>
                        <!--second tab-->
                        <div class="tab-pane" id="members" role="tabpanel">
                            <div class="card-body">
                                <#list model.project.members as member>
                                    <div class="media-body media-text-left">
                                        <h2>${member.userData.user.name}</h2>
                                        <div class="avatar">
                                            <img src="/files/${member.userData.user.photo.storageFileName}" alt="user" style="width: 30px; border-radius: 100%;"/>
                                        </div>
                                    </div>
                                </#list>
                            </div>
                        </div>
                        <!-- next tab-->
                        <div class="tab-pane" id="settings" role="tabpanel">

                        </div>
                    </div>
                </div>
            </div>


            <!-- End PAge Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer" style=" bottom: 0"> © 2018 All rights reserved. Template designed by <a
                href="https://colorlib.com">Colorlib</a></footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->
<!-- All Jquery -->
<script src="/js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="/js/lib/bootstrap/js/popper.min.js"></script>
<script src="/js/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="/js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->


<!-- Amchart -->
<script src="/js/lib/morris-chart/raphael-min.js"></script>
<script src="/js/lib/morris-chart/morris.js"></script>
<script src="/js/lib/morris-chart/dashboard1-init.js"></script>


<script src="/js/lib/calendar-2/moment.latest.min.js"></script>
<!-- scripit init-->
<script src="/js/lib/calendar-2/semantic.ui.min.js"></script>
<!-- scripit init-->
<script src="/js/lib/calendar-2/prism.min.js"></script>
<!-- scripit init-->
<script src="/js/lib/calendar-2/pignose.calendar.min.js"></script>
<!-- scripit init-->
<script src="/js/lib/calendar-2/pignose.init.js"></script>

<script src="/js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="/js/lib/owl-carousel/owl.carousel-init.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/lib/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
<!-- scripit init-->

<script src="/js/custom.min.js"></script>


</body>

</html>