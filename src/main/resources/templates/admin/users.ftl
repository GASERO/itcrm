<body>
<style>
    /* внешние границы таблицы серого цвета толщиной 1px */
    table {border: 1px solid grey;}
    /* границы ячеек первого ряда таблицы */
    th {border: 1px solid grey; text-align: center}
    /* границы ячеек тела таблицы */
    td {border: 1px solid grey;}
</style>
<h4>
    <a href="/admin/users/new">Добавить</a>
</h4>

<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th></th>
    </tr>
<#list model.users as user>
    <tr>
        <td>${user.id}</td>
        <td>${user.user.name}</td>
        <td>
            <form method="post" action="/admin/users/${user.id}/delete">
                <button class = "option">delete</button>
            </form>
        </td>
        <td>
            <a href="/admin/users/${user.id}/update">edit</a>
        </td>
    </tr>
</#list>
</table>
</body>